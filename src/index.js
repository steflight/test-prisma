import "./styles.css";
import "./bootstrap.min.css";
import Parallax from "./parallax";

const { init } = new Parallax("parallax");
init();